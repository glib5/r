# https://stat.ethz.ch/R-manual/R-devel/library/base/html/files.html

DOT <- "."
REGEX_DOT <- "\\."
FORWARD_SLASH <- "/"
BACKSLASH <- "\\"
DOUBLE_BACKSLASH <- base::paste0(BACKSLASH, BACKSLASH) 

easy_split <- function(string, pattern) {
    # convenience basic split function
    base::strsplit(string, pattern)[[1]]
}

format_from_windows_path <- function(path) {
    # assumes the path is NOT a raw string, but that the 
    # chars are escaped manually by the user
    # i.e.:
    # p <- 'c:\\profile\\Desktop\\personal'
    # and not 
    # p <- 'c:\profile\Desktop\personal'
    gsub(DOUBLE_BACKSLASH, FORWARD_SLASH, path)
}

format_to_windows_path <- function(path) {
    gsub(FORWARD_SLASH, DOUBLE_BACKSLASH, path)
}

get_parent_folder <- function(path) {
    base::dirname(format_from_windows_path(path))
}

get_file_name <- function(path) {
    # name = stem + . + extension
    base::basename(format_from_windows_path(path))
}

help_file_name_extract <- function(path, idx) {
    # split a file name on the dot and returns the first or second part
    full_name <- get_file_name(path)
    stem_and_ext <- easy_split(full_name, REGEX_DOT)
    stem_and_ext[[idx]]
}


get_file_extension <- function(path) {
    help_file_name_extract(path, 2)
}

get_file_stem <- function(path) {
    # stem: the filename part without the dot and the extension
    help_file_name_extract(path, 1)
}

easy_paste <- function(str1, str2, collapser) {
    # convenience function to concat 2 strings with a separator
    base::paste(list(str1, str2), collapse = collapser)
}

change_name <- function(path, new_name) {
    parent <- get_parent_folder(path)
    base::file.path(parent, new_name)
}

change_extension <- function(path, new_ext) {
    parent <- get_parent_folder(path)
    stem <- get_file_stem(path)
    new_name <- easy_paste(stem, new_ext, DOT)
    base::file.path(parent, new_name)
}

change_stem <- function(path, new_stem) {
    parent <- get_parent_folder(path)
    ext <- get_file_extension(path)
    new_name <- easy_paste(new_stem, ext, DOT)
    base::file.path(parent, new_name)
}


